var app = require('express')();
var http = require('http').createServer(app);
const { ExpressPeerServer } = require('peer');
const port = 3000;

const peerServer = ExpressPeerServer(http, {
  debug: true,
});

http.listen(port, function () {
  console.log(`listening on *:${port}`);
});

app.use('/', peerServer);
