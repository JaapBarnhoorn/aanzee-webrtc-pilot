import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyA_59GCF_j133HRdi3cpM8ZjbSvVrDN0k8',
  authDomain: 'aanzee-webrtc-f19cf.firebaseapp.com',
  databaseURL: 'https://aanzee-webrtc-f19cf.firebaseio.com',
  projectId: 'aanzee-webrtc-f19cf',
  storageBucket: 'aanzee-webrtc-f19cf.appspot.com',
  messagingSenderId: '576764560126',
  appId: '1:576764560126:web:98a2535fde8aee777b9baf',
};

firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();
export const googleProvider = new firebase.auth.GoogleAuthProvider();

export const db = firebase.firestore();
