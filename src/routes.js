import { wrap } from 'svelte-spa-router';
import Home from './routes/Home.svelte';
import Room from './routes/Room.svelte';
import NotFound from './routes/NotFound.svelte';
import { auth } from './firebase.js';

export const routes = {
  // Exact path
  '/': Home,
  '/room': wrap(Room, (detail) => {
    return auth.onAuthStateChanged((user) => {
      return !!user;
    });
  }),

  // Catch-all
  '*': NotFound,
};
