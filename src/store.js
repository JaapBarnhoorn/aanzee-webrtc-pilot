import { writable } from 'svelte/store';

export const user = writable();
export const showDeviceSettings = writable(false);
