module.exports = {
  theme: {
    container: {
      center: true,
      padding: '2rem',
    },
    fontFamily: {
      sans: ['Roboto', '-apple-system', 'BlinkMacSystemFont', 'sans-serif'],
      display: ['Roboto', 'Oswald', 'sans-serif'],
      body: ['Roboto', 'Open Sans', 'sans-serif'],
    },
    extend: {},
  },
  variants: {},
  plugins: [],
};
